/*
Consider ledPin to be Zappy, for our test circut it's throwing either 5V or nothing at the LED
buttonPin runs to the footpeddle
timeUp and timeDown run to the two onboard buttons; 
I figure you're gonna want the default timers and +- to be slightly different. 

*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 7;     // the number of the pushbutton pin
const int ledPin =  9;      // the number of the LED pin
const int timeUpPin = 5;
const int timeDownPin = 2; 
const int otherLedPin = 13;

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
int upState = 0;
int downState =0;

long interval = 1000;

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  pinMode(otherLedPin, OUTPUT);
  
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  pinMode(timeUpPin, INPUT);
  pinMode(timeDownPin, INPUT); 
  
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  upState = digitalRead(timeUpPin);
  downState = digitalRead(timeDownPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on, wait interval then off:
    digitalWrite(ledPin, HIGH);
    delay (interval); 
    digitalWrite(ledPin, LOW);
    delay (1000);
  } 
  
  if (upState == HIGH) {
    // turn up the interval
    digitalWrite(otherLedPin, HIGH);   
    interval += 100;
    delay(200);
    if (interval >= 30000){
      interval = 0;
    }
    digitalWrite(otherLedPin, LOW);
  }
  
  if (downState == HIGH) {
    digitalWrite(otherLedPin, HIGH);
    interval -= 100;
    delay(200);
    if (interval <=0){
     interval = 30000; 
    }
    digitalWrite(otherLedPin, LOW);
  }

/*
 * I haven't figured out how to get the neopixels to display yet, but minimally you can always reset the board
*/

}
